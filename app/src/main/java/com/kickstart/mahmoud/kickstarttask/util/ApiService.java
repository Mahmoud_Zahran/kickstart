package com.kickstart.mahmoud.kickstarttask.util;

import com.kickstart.mahmoud.kickstarttask.model.SpreadSheet;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiService {
    @GET(ApiUrls.API_FEEDS_URL)
    Observable<SpreadSheet> getApiData();
}
