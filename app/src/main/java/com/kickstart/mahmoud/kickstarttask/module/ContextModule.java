package com.kickstart.mahmoud.kickstarttask.module;


import android.content.Context;

import com.kickstart.mahmoud.kickstarttask.qualifier.ApplicationContext;
import com.kickstart.mahmoud.kickstarttask.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context){
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context providesContext(){
        return context;
    }
}