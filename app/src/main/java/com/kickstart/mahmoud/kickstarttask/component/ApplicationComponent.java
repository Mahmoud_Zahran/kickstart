package com.kickstart.mahmoud.kickstarttask.component;

import android.content.Context;

import com.kickstart.mahmoud.kickstarttask.module.ContextModule;
import com.kickstart.mahmoud.kickstarttask.module.RetrofitModule;
import com.kickstart.mahmoud.kickstarttask.root.MyApplication;
import com.kickstart.mahmoud.kickstarttask.qualifier.ApplicationContext;
import com.kickstart.mahmoud.kickstarttask.scope.ApplicationScope;
import com.kickstart.mahmoud.kickstarttask.util.ApiService;

import dagger.Component;

@ApplicationScope
@Component(modules = {ContextModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    ApiService getApiService();

    @ApplicationContext
    Context getContext();

    void injectApplication(MyApplication application);
}