package com.kickstart.mahmoud.kickstarttask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpreadSheet {
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("encoding")
    @Expose
    private String encoding;
    @SerializedName("feed")
    @Expose
    feed feed;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public feed getFeed() {
        return feed;
    }

}
