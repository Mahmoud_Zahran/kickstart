package com.kickstart.mahmoud.kickstarttask.module;

import android.content.Context;

import com.kickstart.mahmoud.kickstarttask.mainActivityMVP.MainActivity;
import com.kickstart.mahmoud.kickstarttask.qualifier.ActivityContext;
import com.kickstart.mahmoud.kickstarttask.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityContextModule {


    private MainActivity mainActivity;
    private Context context;

    public MainActivityContextModule(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        context = mainActivity;
    }

    @Provides
    @ActivityScope
    public MainActivity providesMainActivity(){
        return mainActivity;
    }

    @Provides
    @ActivityScope
    @ActivityContext
    public Context providesContext(){
        return context;
    }
}