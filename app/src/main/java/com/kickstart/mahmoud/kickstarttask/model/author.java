package com.kickstart.mahmoud.kickstarttask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class author {
    @SerializedName("rel")
    @Expose
    name name;
    @SerializedName("type")
    @Expose
    email email;

    public name getName() {
        return name;
    }

    public void setName(name name) {
        this.name = name;
    }

    public email getEmail() {
        return email;
    }

    public void setEmail(email email) {
        this.email = email;
    }
}
