package com.kickstart.mahmoud.kickstarttask.component;


import android.content.Context;

import com.kickstart.mahmoud.kickstarttask.module.MainActivityContextModule;
import com.kickstart.mahmoud.kickstarttask.module.MainActivityMvpModule;
import com.kickstart.mahmoud.kickstarttask.mainActivityMVP.MainActivity;
import com.kickstart.mahmoud.kickstarttask.qualifier.ActivityContext;
import com.kickstart.mahmoud.kickstarttask.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(modules = {MainActivityContextModule.class, MainActivityMvpModule.class},
        dependencies = ApplicationComponent.class)
public interface MainActivityComponent {

    @ActivityContext
    Context getContext();
    void injectMainActivity(MainActivity mainActivity);
}