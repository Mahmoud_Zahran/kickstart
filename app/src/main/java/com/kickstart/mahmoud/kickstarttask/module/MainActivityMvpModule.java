package com.kickstart.mahmoud.kickstarttask.module;

import com.kickstart.mahmoud.kickstarttask.mainActivityMVP.MainActivityContract;
import com.kickstart.mahmoud.kickstarttask.mainActivityMVP.MainActivityPresenterImpl;
import com.kickstart.mahmoud.kickstarttask.scope.ActivityScope;
import com.kickstart.mahmoud.kickstarttask.util.ApiService;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityMvpModule {

    private MainActivityContract.View mView;

    public MainActivityMvpModule(MainActivityContract.View mView){
        this.mView = mView;
    }

    @Provides
    @ActivityScope
    MainActivityContract.View provideView(){
        return mView;
    }

    @Provides
    @ActivityScope
    MainActivityPresenterImpl providePresenter(ApiService feedsApi, MainActivityContract.View mView){
        return new MainActivityPresenterImpl(feedsApi, mView);
    }

}
