package com.kickstart.mahmoud.kickstarttask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class feed {

    @SerializedName("xmlns")
    @Expose
    private String xmlns;
    @SerializedName("xmlns$openSearch")
    @Expose
    private String xmlns$openSearch;
    @SerializedName("xmlns$gsx")
    @Expose
    private String xmlns$gsx;
    @SerializedName("id")
    @Expose
    id id;
    @SerializedName("updated")
    @Expose
    updated updated;
    @SerializedName("category")
    @Expose
    ArrayList<category> categories;
    @SerializedName("title")
    @Expose
    title title;
    @SerializedName("link")
    @Expose
    ArrayList<link> links;

    @SerializedName("author")
    @Expose
    ArrayList<author> authors;

    @SerializedName("openSearch$totalResults")
    @Expose
    openSearch$totalResults openSearch$totalResults;


    @SerializedName("openSearch$startIndex")
    @Expose
    openSearch$startIndex openSearch$startIndex;

    @SerializedName("entry")
    @Expose
    ArrayList<entry> entries;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getXmlns$openSearch() {
        return xmlns$openSearch;
    }

    public void setXmlns$openSearch(String xmlns$openSearch) {
        this.xmlns$openSearch = xmlns$openSearch;
    }

    public String getXmlns$gsx() {
        return xmlns$gsx;
    }

    public void setXmlns$gsx(String xmlns$gsx) {
        this.xmlns$gsx = xmlns$gsx;
    }

    public id getId() {
        return id;
    }

    public void setId(id id) {
        this.id = id;
    }

    public updated getUpdated() {
        return updated;
    }

    public void setUpdated(updated updated) {
        this.updated = updated;
    }

    public ArrayList<category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<category> categories) {
        this.categories = categories;
    }

    public title getTitle() {
        return title;
    }

    public void setTitle(title title) {
        this.title = title;
    }

    public ArrayList<link> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<link> links) {
        this.links = links;
    }

    public ArrayList<author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<author> authors) {
        this.authors = authors;
    }

    public openSearch$totalResults getOpenSearch$totalResults() {
        return openSearch$totalResults;
    }

    public void setOpenSearch$totalResults(openSearch$totalResults openSearch$totalResults) {
        this.openSearch$totalResults = openSearch$totalResults;
    }

    public openSearch$startIndex getOpenSearch$startIndex() {
        return openSearch$startIndex;
    }

    public void setOpenSearch$startIndex(openSearch$startIndex openSearch$startIndex) {
        this.openSearch$startIndex = openSearch$startIndex;
    }

    public ArrayList<entry> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<entry> entries) {
        this.entries = entries;
    }
}
