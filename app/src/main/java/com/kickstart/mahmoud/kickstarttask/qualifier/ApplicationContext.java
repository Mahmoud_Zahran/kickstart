package com.kickstart.mahmoud.kickstarttask.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
