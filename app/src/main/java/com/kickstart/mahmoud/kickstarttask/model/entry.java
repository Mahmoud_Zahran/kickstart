package com.kickstart.mahmoud.kickstarttask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class entry {
    @SerializedName("id")
    @Expose
    id id;
    @SerializedName("updated")
    @Expose
    updated updated;
    @SerializedName("category")
    @Expose
    ArrayList<category> categories;
    @SerializedName("title")
    @Expose
    title title;
    @SerializedName("link")
    @Expose
    ArrayList<link> links;
    @SerializedName("content")
    @Expose
    content content;

    public id getId() {
        return id;
    }

    public void setId(id id) {
        this.id = id;
    }

    public updated getUpdated() {
        return updated;
    }

    public void setUpdated(updated updated) {
        this.updated = updated;
    }

    public ArrayList<category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<category> categories) {
        this.categories = categories;
    }

    public title getTitle() {
        return title;
    }

    public void setTitle(title title) {
        this.title = title;
    }

    public ArrayList<link> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<link> links) {
        this.links = links;
    }

    public content getContent() {
        return content;
    }

    public void setContent(content content) {
        this.content = content;
    }
}
