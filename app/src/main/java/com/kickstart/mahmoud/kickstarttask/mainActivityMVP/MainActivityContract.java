package com.kickstart.mahmoud.kickstarttask.mainActivityMVP;

import com.kickstart.mahmoud.kickstarttask.model.MessageDetails;

import java.util.ArrayList;


//This is the blueprint of View-Model-Presenter.

public interface MainActivityContract {
    interface View{

        void showError(String call, String statusMessage);
        void showProgress();
        void hideProgress();
        void showComplete();
        void updateMarker(ArrayList<MessageDetails> messages);
    }

    interface Presenter{
        void loadFeedsData();
        ArrayList<MessageDetails> getMessages();
        ArrayList<MessageDetails> getPositiveMessages();
        ArrayList<MessageDetails> getNegativeMessages();
        ArrayList<MessageDetails> getNautralMessages();
    }
}
