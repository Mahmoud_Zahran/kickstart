package com.kickstart.mahmoud.kickstarttask.mainActivityMVP;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kickstart.mahmoud.kickstarttask.R;
import com.kickstart.mahmoud.kickstarttask.component.DaggerMainActivityComponent;
import com.kickstart.mahmoud.kickstarttask.module.MainActivityContextModule;
import com.kickstart.mahmoud.kickstarttask.module.MainActivityMvpModule;
import com.kickstart.mahmoud.kickstarttask.model.MessageDetails;
import com.kickstart.mahmoud.kickstarttask.component.ApplicationComponent;
import com.kickstart.mahmoud.kickstarttask.component.MainActivityComponent;
import com.kickstart.mahmoud.kickstarttask.root.MyApplication;
import com.kickstart.mahmoud.kickstarttask.qualifier.ActivityContext;
import com.kickstart.mahmoud.kickstarttask.qualifier.ApplicationContext;
import com.kickstart.mahmoud.kickstarttask.util.CustomProgressDialog;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//, OnMapReadyCallback, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerDragListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener
public class MainActivity extends FragmentActivity implements MainActivityContract.View, OnMapReadyCallback {
    @Inject
    @ApplicationContext
    public Context context;

    @Inject
    @ActivityContext
    public Context activityContext;

    @Inject
    MainActivityPresenterImpl mainActivityPresenter;
    String TAG = "MainActivity";
    @BindView(R.id.negative_checkBox)
    CheckBox negativeCheckBox;
    @BindView(R.id.positive_checkBox)
    CheckBox positiveCheckBox;
    @BindView(R.id.neutral_checkBox)
    CheckBox neutralCheckBox;

    private GoogleMap mMap;

    Marker marker;
    ArrayList<MessageDetails> allmessagesArrayList;
    ArrayList<MessageDetails> mergedMessages;
    CustomProgressDialog mCustomProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //    ApiMethods.getFeeds(this,false);
        MainActivityComponent mainActivityComponent;


        ButterKnife.bind(this);
        ApplicationComponent applicationComponent = MyApplication.get(this).getApplicationComponent();
        mainActivityComponent = DaggerMainActivityComponent.builder()
                .mainActivityContextModule(new MainActivityContextModule(this))
                .mainActivityMvpModule(new MainActivityMvpModule(this))
                .applicationComponent(applicationComponent)
                .build();
        mainActivityComponent.injectMainActivity(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mainActivityPresenter.loadFeedsData();

    }


    @Override
    public void showError(String call, String statusMessage) {
        if (call.equals("network error")) {
            Log.d(TAG, "showError:network error " + statusMessage);
            Toast.makeText(context, statusMessage, Toast.LENGTH_SHORT).show();
        } else {
            Log.d(TAG, "showError: " + "error message");
            Toast.makeText(context, "error message", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showProgress() {
        mCustomProgressDialog = new CustomProgressDialog(this);
        mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mCustomProgressDialog.show();
        Log.d(TAG, "showProgress: " + "showProgress message");
        //  Toast.makeText(context, "showProgress message", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void hideProgress() {
        mCustomProgressDialog.dismiss();
        mCustomProgressDialog = null;
        Log.d(TAG, "hideProgress: " + "hideProgress message");
        //   Toast.makeText(context, "hideProgress message", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showComplete() {
        allmessagesArrayList = mainActivityPresenter.getMessages();
        Log.d(TAG, "showComplete: " + "showComplete message");
//        Toast.makeText(context, "showComplete message", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
//        context.startActivity(intent);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));

        //  mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void updateMarker(ArrayList<MessageDetails> messages) {


        if (marker != null) {
            marker.remove();
            mMap.clear();
        }
        for (MessageDetails messageDetails : messages) {

            if (messageDetails.getSentiment().trim().equals("Positive")) {
                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.positive);
                Bitmap b = bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);
                marker = mMap.addMarker(new MarkerOptions().
                        position(messageDetails.getLatLng()).
                        title(messageDetails.getMessage() + "," + messageDetails.getSentiment()).
                        icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

            } else if (messageDetails.getSentiment().trim().equals("Neutral")) {
                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.neutral);
                Bitmap b = bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);
                marker = mMap.addMarker(new MarkerOptions().
                        position(messageDetails.getLatLng()).
                        title(messageDetails.getMessage() + "," + messageDetails.getSentiment()).
                        icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

            } else if (messageDetails.getSentiment().trim().equals("Negative")) {
                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.negative);
                Bitmap b = bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);
                marker = mMap.addMarker(new MarkerOptions().
                        position(messageDetails.getLatLng()).
                        title(messageDetails.getMessage() + "," + messageDetails.getSentiment()).
                        icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

            }


            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(messageDetails.getLatLng()).tilt(45).bearing(45).zoom((float) 3).build();


            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            //p.finishPulse();
            // p.setVisibility(View.GONE);
        }

    }

    @OnClick({R.id.negative_checkBox, R.id.positive_checkBox, R.id.neutral_checkBox})

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.negative_checkBox:
                mergedMessages= new ArrayList<>();
                if (negativeCheckBox.isChecked())
                     mergedMessages.addAll(mainActivityPresenter.getNegativeMessages());
                if (positiveCheckBox.isChecked())
                     mergedMessages.addAll(mainActivityPresenter.getPositiveMessages());
                if (neutralCheckBox.isChecked())
                     mergedMessages.addAll(mainActivityPresenter.getNautralMessages());

               updateMarker(mergedMessages);


                break;
            case R.id.positive_checkBox:
                mergedMessages= new ArrayList<>();
                if (negativeCheckBox.isChecked())
                    mergedMessages.addAll(mainActivityPresenter.getNegativeMessages());
                if (positiveCheckBox.isChecked())
                    mergedMessages.addAll(mainActivityPresenter.getPositiveMessages());
                if (neutralCheckBox.isChecked())
                    mergedMessages.addAll(mainActivityPresenter.getNautralMessages());
                updateMarker(mergedMessages);

                break;
            case R.id.neutral_checkBox:
                mergedMessages= new ArrayList<>();
                if (negativeCheckBox.isChecked())
                    mergedMessages.addAll(mainActivityPresenter.getNegativeMessages());
                if (positiveCheckBox.isChecked())
                    mergedMessages.addAll(mainActivityPresenter.getPositiveMessages());
                if (neutralCheckBox.isChecked())
                    mergedMessages.addAll(mainActivityPresenter.getNautralMessages());
                updateMarker(mergedMessages);

                break;

        }
    }
//    private void updateDriverMarker(ArrayList<MessageDetails> messages) {
//
//        if (drivermarker != null) {
//            drivermarker.remove();
//        }
//        for (MessageDetails messageDetails:messages) {
//
//            if (messageDetails.getSentiment().trim().equals("Positive")){
//                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.positive);
//                Bitmap b = bitmapdraw.getBitmap();
//                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);
//                drivermarker = mMap.addMarker(new MarkerOptions().
//                        position(messageDetails.getLatLng()).
//                        title(messageDetails.getMessage() + "," + messageDetails.getSentiment()).
//                        icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
//
//            }else if (messageDetails.getSentiment().trim().equals("Neutral")){
//                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.neutral);
//                Bitmap b = bitmapdraw.getBitmap();
//                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);
//                drivermarker = mMap.addMarker(new MarkerOptions().
//                        position(messageDetails.getLatLng()).
//                        title(messageDetails.getMessage() + "," + messageDetails.getSentiment()).
//                        icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
//
//            }else if (messageDetails.getSentiment().trim().equals("Negative")){
//                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.negative);
//                Bitmap b = bitmapdraw.getBitmap();
//                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 60, 60, false);
//                drivermarker = mMap.addMarker(new MarkerOptions().
//                        position(messageDetails.getLatLng()).
//                        title(messageDetails.getMessage() + "," + messageDetails.getSentiment()).
//                        icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
//
//            }
//
//
//            CameraPosition cameraPosition = new CameraPosition.Builder()
//                    .target(messageDetails.getLatLng()).tilt(45).bearing(45).zoom((float) 3).build();
//
//
//            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            //p.finishPulse();
//           // p.setVisibility(View.GONE);
//        }
//
//    }
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
//    @Override
//    public void onMapLongClick(LatLng latLng) {
//
//    }
//
//    @Override
//    public boolean onMarkerClick(Marker marker) {
//        return false;
//    }
//
//    @Override
//    public void onMarkerDragStart(Marker marker) {
//
//    }
//
//    @Override
//    public void onMarkerDrag(Marker marker) {
//
//    }
//
//    @Override
//    public void onMarkerDragEnd(Marker marker) {
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//    }
}
