package com.kickstart.mahmoud.kickstarttask.mainActivityMVP;

import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.kickstart.mahmoud.kickstarttask.model.MessageDetails;
import com.kickstart.mahmoud.kickstarttask.model.SpreadSheet;
import com.kickstart.mahmoud.kickstarttask.model.entry;
import com.kickstart.mahmoud.kickstarttask.root.MyApplication;
import com.kickstart.mahmoud.kickstarttask.util.ApiService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityPresenterImpl implements MainActivityContract.Presenter  {

    private ApiService feedsApi;
    private MainActivityContract.View mView;

    SpreadSheet mSpreadSheet;
    ArrayList<MessageDetails> mAllMessages;
    ArrayList<MessageDetails> mPositiveMessages;
    ArrayList<MessageDetails> mNegativeMessages;
    ArrayList<MessageDetails> mNautralMessages;


    @Inject
    public MainActivityPresenterImpl(ApiService feedsApi, MainActivityContract.View mView){
        this.feedsApi = feedsApi;
        this.mView = mView;


    }

    @Override
    public void loadFeedsData() {
        mView.showProgress();

        feedsApi.getApiData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SpreadSheet>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(SpreadSheet spreadSheet) {
                        if(spreadSheet != null){
                            if(spreadSheet.getFeed().getEntries().get(0).getContent().get$t()!= null){
                                mSpreadSheet=spreadSheet;
                                mView.showComplete();

                            }else{
                                mView.showError("message null", spreadSheet.getVersion());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError("network error","Error occurred");
                        Log.e("Login", e.getMessage(), e);
                        mView.hideProgress();
                    }


                    @Override
                    public void onComplete() {
                        if (mSpreadSheet!= null){
                             mView.updateMarker(mAllMessages);
                        }
                        mView.hideProgress();
                    }

                });
    }

    @Override
    public ArrayList<MessageDetails> getMessages() {


        mAllMessages=new ArrayList<>();
        mPositiveMessages= new ArrayList<>();
        mNegativeMessages= new ArrayList<>();
        mNautralMessages=new ArrayList<>();


        for (entry entry : mSpreadSheet.getFeed().getEntries()) {
            MessageDetails message=new MessageDetails();

            message.setMessageid(entry.getContent().get$t().split(",")[0].split(":")[1]!=null?
                    entry.getContent().get$t().split(",")[0].split(":")[1]:"null");
            message.setMessage(entry.getContent().get$t().split(", message:")[1].
                    split(", sentiment:")[0]);

            message.setSentiment(entry.getContent().get$t().split(", message:")[1].
                    split(", sentiment:")[1]);
            Log.i("MainActivityPresenterImpl", "getMessages: "+ message.getMessage()+message.getSentiment());
            List<Address> addressList = null;
            Geocoder geocoder=new Geocoder(MyApplication.getContext());
            try {
                addressList=geocoder.getFromLocationName(message.getMessage(),1);

            } catch (IOException e) {
                e.printStackTrace();
            }
            LatLng latLng;
            if (addressList!= null && addressList.size()>0) {
                Address address = addressList.get(0);
                 latLng= new LatLng(address.getLatitude(), address.getLongitude());
            }
            else{
                latLng=new LatLng(0.0,0.0);

            }
            message.setLatLng(latLng);

            if (message.getSentiment().trim().equals("Positive")){
                mPositiveMessages.add(message);
            }else if (message.getSentiment().trim().equals("Negative")){
                mNegativeMessages.add(message);
            }else if (message.getSentiment().trim().equals("Neutral")){
                mNautralMessages.add(message);
            }
            mAllMessages.add(message);
        }
        return mAllMessages;

    }

    @Override
    public ArrayList<MessageDetails> getPositiveMessages() {

        return mPositiveMessages != null ? mPositiveMessages : getMessages();

    }

    @Override
    public ArrayList<MessageDetails> getNegativeMessages() {
        return mNegativeMessages != null ? mNegativeMessages : getMessages();
    }

    @Override
    public ArrayList<MessageDetails> getNautralMessages() {
        return mNautralMessages != null ? mNautralMessages : getMessages();
    }
}
