package com.kickstart.mahmoud.kickstarttask.root;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.kickstart.mahmoud.kickstarttask.component.ApplicationComponent;
import com.kickstart.mahmoud.kickstarttask.component.DaggerApplicationComponent;
import com.kickstart.mahmoud.kickstarttask.module.ContextModule;


public class MyApplication extends Application {
    ApplicationComponent applicationComponent;
    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        dependencyInjection();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static MyApplication get(Activity activity) {
        return (MyApplication) activity.getApplication();
    }
    public static Context getContext(){
        return mContext;
    }

    private void dependencyInjection(){
        applicationComponent = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this)).build();
        applicationComponent.injectApplication(this);
    }
}