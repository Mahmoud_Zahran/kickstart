package com.kickstart.mahmoud.kickstarttask.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageDetails {
    @SerializedName("sentiment")
    @Expose
    private String sentiment;
    @SerializedName("messageid")
    @Expose
    private String messageid;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("latLng")
    @Expose
    private LatLng latLng;

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public String getMessageid() {
        return messageid;
    }

    public void setMessageid(String messageid) {
        this.messageid = messageid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
